import pytest

"""Tests currently focus on smoke testing basic functionality at the api level
"""
from app.routes import app


@pytest.fixture
def client():
    """bootstrap client for testing
    """
    app.testing = True
    client = app.test_client()

    yield client


def test_unknown_route_handler(client):
    """Testing the 404 route handler"""

    response = client.get('/')
    assert response.status_code == 404
    assert response.is_json
    assert 'error' in response.get_json()


def test_team_profile(client):
    """Simple test for team profile"""

    response = client.get(
        '/teams/mailchimp?bitbucket=mailchimp&github=mailchimp')
    assert response.status_code == 200
    assert response.is_json
