"""
    Normally I would focus on mocking the service response but I think 
    the effort and setup time would hinder completion in a timely manner
    This testing relies on the actual services being accessible which 
    would indicate more of an end to end test here
"""
import pytest
from app.dal import BitbucketDAO
from app.models import Team
from app.tests import pretty_print


def test_bitbucket_repository():
    client = BitbucketDAO()
    team = Team(
        name='mailchimp', github_team='mailchimp', bitbucket_team='mailchimp')
    response = client.repositories('mailchimp')
