import pytest

from app.models import Team


def test_TeamProfile_instantiation():
    team = Team(name='mailchimp', bitbucket_team='mailchimp',
                github_team='mailchimp')
    assert type(team) == Team
    assert team.bitbucket_team == 'mailchimp'
    assert team.github_team == 'mailchimp'
