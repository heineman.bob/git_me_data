"""Module level convenience methods
"""
import pprint
pretty_print = pprint.PrettyPrinter(indent=4).pprint
