import attr
import json
import logging

import flask
from flask import Response, jsonify, request, escape

from .models import Team

app = flask.Flask('user_profiles_api')
logger = flask.logging.create_logger(app)
logger.setLevel(logging.DEBUG)


@app.route('/health-check', methods=['GET'])
def health_check():
    """
    Endpoint to health check API

    Notes:  I would follow this up with either dependent service 
            information in the future or add an additional route 
            to host that information.
    """
    app.logger.info('Health Check!')
    return Response('All Good!', status=200)


@app.route('/teams/<name>', methods=['GET'])
def get_team_profile(name):
    """Retrieve team profile from providers

    Arguments:
        name {str} -- name of team profile

    Query Parameters:
        github -- name of the org you want to query on github
        bitbucket -- name of the team you want to query on bitbucket

    Returns:
        json -- document which represents a merged view of 
                github and bitbucket information

    Notes:
        If you only provide the name resource and no query parameters
        then subsequent api calls will be made using this name on each service
        respectively.

        Optionally if you provide one or both query parameters these will override
        this default functionality.
    """
    github_team = request.args.get('github')
    bitbucket_team = request.args.get('bitbucket')
    team = Team(escape(name), bitbucket_team=bitbucket_team,
                github_team=github_team)

    return jsonify(attr.asdict(team))


@app.errorhandler(404)
def page_not_found(e):
    """404 Handler for rogue route requests

    Arguments:
        e {Exception} -- code_or_exception

    Returns:
        Response -- json response with error message
    """
    return jsonify(error=str(e)), 404
