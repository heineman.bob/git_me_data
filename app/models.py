"""In this class I'm using the attrs package which is one of my favorite

    I didn't want to use this everywhere because of this being an exercise to show depth of knowledge
    but I did want to display the beautify of this here.  It cuts down on tons of boiler plate code.

    https://github.com/python-attrs/attrs

"""
import attr
import json
from .dal import BitbucketDAO, GithubDAO


@attr.s
class Team(object):
    """Model to represent merged information from multiple git
       services
    """
    name = attr.ib(type=str)
    bitbucket_team = attr.ib(type=str)
    github_team = attr.ib(type=str)
    watcher_count = attr.ib(init=False, type=int, default=0)
    languages = attr.ib(init=False, type=dict, default=attr.Factory(dict))
    topics = attr.ib(init=False, type=dict, default=attr.Factory(dict))
    repository_stats = attr.ib(type=dict, default=attr.Factory(dict))

    def __attrs_post_init__(self):
        """In attrs this acts as the area for custom init code
            This defaults the passed in name to both bitbucket
            and github team names.
        """
        if not self.bitbucket_team:
            self.bitbucket_team = self.name

        if not self.github_team:
            self.github_team = self.name

        self.repository_stats = {
            'total': 0,
            'original': 0,
            'forked': 0
        }

        self.populate_bitbucket_info()
        self.populate_github_info()
        self.calculate_total_repo_count()

    def increment_repo_type(self, fork_keyword, keys):
        """builds repository metadata for forking information

        Arguments:
            fork_keyword {str} -- keyword to search keys for
            keys {list} -- keys to search for keyword

        Notes:
            Each service may have their own way of identifying
            whether or not a repository is forked or original.
            This allows an abstraction of that logic and flexibility
            to adapt to multiple services.
        """
        if fork_keyword in keys:
            self.repository_stats['forked'] += 1
        else:
            self.repository_stats['original'] += 1

    def add_language(self, language):
        """add a language to the languages dict

        Arguments:
            language {str} -- language to add
        """
        language = language.lower()
        if language not in self.languages.keys():
            self.languages[language] = 1
        else:
            self.languages[language] += 1

    def populate_bitbucket_info(self):
        """Adds metadata to the class and provides an area
           for custom business logic around bitbuckets data
        """
        client = BitbucketDAO()
        repositories = client.repositories(self.bitbucket_team)

        for repo in repositories:
            self.increment_repo_type('parent', repo.keys())

            self.watcher_count += client.watcher_count(
                repo['links']['watchers']['href'])

            self.add_language(repo['language'])

    def populate_github_info(self):
        """Adds metadata to the class and provides an area
           for custom business logic around githubs data
        """
        client = GithubDAO()
        repositories = client.repositories((self.github_team))

        for repo in repositories:
            self.increment_repo_type('parent', repo.keys())

            if 'topics' in repo.keys():
                for topic in repo['topics']:
                    if topic not in self.topics.keys():
                        self.topics[topic] = 1
                    else:
                        self.topics[topic] += 1

            if 'language' in repo.keys() and isinstance(repo['language'], str):
                self.add_language(repo['language'])

            self.watcher_count += repo['watchers_count']

    def calculate_total_repo_count(self):
        """Update total counts instead of making N number of calls to increment
        """
        self.repository_stats['total'] += self.repository_stats['forked'] + \
            self.repository_stats['original']
