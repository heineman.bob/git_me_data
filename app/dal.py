"""Data Access Layer

   While this is primitively organized in this application I would
   move this to a separate module for larger applications and break out different
   categories of data providers
"""
import requests


class RestUtilMixin(object):
    """Mixin to add common rest functionality   

    Raises:
        ValueError: raise error if team name is not found

    Returns:
        [type] -- [description]
    """

    def entity_not_found(self, entity):
        return {
            'error': f'{entity} was not found in the bitbucket api'
        }

    def _get_request(self, uri, **kwargs):
        response = requests.get(uri, **kwargs)

        if response.status_code == 404:
            raise ValueError

        return response


class BitbucketDAO(RestUtilMixin, object):
    """Used to interface with bitbucket api service
    """

    def __init__(self, endpoint='https://api.bitbucket.org/2.0', user='slmd2k3', app_password='YqyNpRzUGzaQK6NVBfen'):
        self._endpoint = endpoint
        self._auth = (user, app_password)

    def repositories(self, username_or_uuid):
        """Get repositories

        Arguments:
            username_or_uuid {str} -- either be the username or the UUID of the account

        Notes:
            If a team name is provided and not found instead of raising an exception this 
            returns an empty list that will be processed and moves on to the next.

            In the future I would prossible add a multipart response for this or add an error or
            messages field in the response.  This is really dependent on the users of this api and
            what they want to do with this information.  If it was critical that they get an accurate response
            I would give back only errors with relevant information so they could update their calling code.

            Again I think more context here would be needed to make a better decision.
        """

        repo_list_uri = f'{self._endpoint}/repositories/{username_or_uuid}?q=is_private=false'

        try:
            response = self._get_request(
                repo_list_uri, auth=self._auth).json()
            repositories = []

            if 'next' not in response.keys():
                return response['values']
            else:
                while 'next' in response.keys():
                    response = self.__get_request(
                        response['next'], auth=self._auth).json()
                    repositories = repositories + response['values']

            return repositories
        except ValueError:
            return []

    def watcher_count(self, href):
        return self._get_request(href).json()['size']


class GithubDAO(RestUtilMixin, object):
    """Interacts with the github api"""

    def __init__(self, endpoint='https://api.github.com', user='heineman-bob', app_password='5a30e329f1fb86f0ebba1eee82972fa738a2e161'):
        self._endpoint = endpoint
        self._auth = (user, app_password)

    def repositories(self, org_name):
        """Get repositories from github for org

        Arguments:
            org_name {str} -- name of organization to get repositories
        """

        def _extract_next_link(headers):
            """extract next link if it exists

            Arguments:
                headers {dict} -- headers and values for request

            Returns:
                str -- Link to next page of repositories or None to signify the end

            Notes:
                This nested method is structured as such to prevent outside use.  
                This is currently restricted to only this method.  If additional functions needed something
                similar I would consider either moving this to the DAO class or perhaps even it's own mixin depending
                on the next slice of functionality.
            """
            if 'Link' not in headers.keys():
                return None

            try:
                # TODO: clean up this search feature
                next_link = next(x for x in headers['Link'].split(
                    ',') if 'rel="next"' in x)

                if next_link:
                    return next_link.split(';')[0].strip('<')
                else:
                    return None
            except StopIteration:
                return None

        try:
            repo_list_uri = f'{self._endpoint}/orgs/{org_name}/repos?type=public'
            response = self._get_request(repo_list_uri, auth=self._auth)

            repositories = response.json()
            next_link = _extract_next_link(response.headers)

            while(next_link):
                response = self._get_request(next_link, auth=self._auth)
                next_link = _extract_next_link(response.headers)
                repositories = repositories + response.json()

            return repositories

        except ValueError:
            return []
