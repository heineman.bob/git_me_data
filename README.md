# Coding Challenge App

A skeleton flask app to use for a coding challenge.

## Authors note

I tried to show breadth of knowledge while not leveraging packages that just consumed the bitbucket and github api. I thought that might be cheating and there is definitely benefit to managing your own package when consuming downstream services. I also noticed that github offered a graphql option and that would be ideal to optimize network traffic and eliminate cruft metadata. If I had more time this could be much cleaner but I think I like where this is at so I will stop here and would be happy to discuss any of this further if anyone deems this code worthy.

## Requirements

[Docker](https://docs.docker.com/install/)  
[Docker Compose](https://docs.docker.com/compose/install/)

_While not a requirement for this challenge I find that this setup allows developers to quickly iterate in an isolated development environment while still retaining granular control. This also eliminates the need for virtual environments and allows injection of env vars based on a file vs. your own env vars._

## Quick Start

```
docker-compose up
```

After this you can make changes to your local code and the development server will update your flask application since this is configured for debug mode and docker-compose mounts the source code as a volume.

## Development workflow

1. use the `docker-compose up` command to start your api in development mode
2. In a separate terminal type `docker-compose run api ptw app`
   - this will start pytest-watch which runs the test suite on file change
3. Update local codebase and call service

This basic strategy ensures continous local testing and will throw errors if you break things.

## Testing

Tests are located in the tests module

```
# Execute the tests
docker-compose run api pytest tests

# Continuously test
docker-compose run api ptw tests
```

## Making Requests

```
curl -i "http://127.0.0.1:5000/health-check"
```

## Building release images

```
docker build .
```

Since tags are mutable we don't need to bother with them here. At the end of the build we will grab that sha and leverage that in any staged manifests.

## What'd I'd like to improve on...

- requirements.txt broken into multiple parts to breakout testing pieces then leverage docker multistage builds to build test only images
- error handling / input validation (with a short time I felt moving on to the broader functionality was more important given the time constraints)
- secret injection via config using env vars and flask config. I would normally have these in kubernetes secret vault and inject them in via deployment.yml files.
- logging

## Testing

_I would say I bootstrapped the testing framework with ptw and pytest and even implemented a gitlab ci file to test on checkin but I didn't really do test driven development here like I would have liked to. I found myself exploring the api docs and turning a proof of concept into the final product._

My testing strategy:

Begin at the unit test level for the model and ensure input validation was up to snuff and all user parameters were escaped properly to avoid any type of sqlinjection.
Check for null cases, check for edge cases (type limits etc..)
After dal and models have been tested I would perform integration testing using the test_api.py file and mock out service replies to simulate healthy and unhealthy dependent services.
Most importantly my integration tests would test the contract defined for users.

## Security

Currently I have app tokens with only read access hardcoded in the application but this was only for times sake and would never go into anything that is important. I really want to stress that :)

## Optimization

Currently this is just a stateless api. I think to improve functionality I might implement a cache using redis or postgres/mysql to store retreived information.
