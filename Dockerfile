# I'm going to use the alpine based image to reduce the image footprint
FROM python:3.6-alpine 
LABEL author="Robert Heineman"

WORKDIR /opt/api/
COPY . .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD [ "python", "run.py" ]